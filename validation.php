<?php
session_start();

function checkIfEmptyEmail() {
    $email = $_POST['email'];
    if (empty($_POST['email'])) {
        $_SESSION['emailEr'] = "<p style='color:red'>Please write your email</p>";
        return false;
    } else {
        return true;
    } 
        
}

function validateInputEmail() {
    if (checkIfEmptyEmail() == true) {
        $email = $_POST['email'];
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return true;
        } else {
            $_SESSION['emailError'] = "<p style='color:red'>Invalid email input</p>";
            return false;
        }
    }
}

function checkIfEmptyCompany() {

    $name = $_POST['name'];
    $website = $_POST['website'];
    $about = $_POST['about'];
    $email = $_POST['email'];
    
        if (empty($name)) {
            $_SESSION['name'] = "<p style='color:red'>Company name is required</p>";
            header('Location: form.php');
            die();
        } 

        if (empty($website)) {
            $_SESSION['website'] = "<p style='color:red'>Website is required</p>";
            header('Location: form.php');
            die();
        } 

        if (empty($about)) {
            $_SESSION['about'] = "<p style='color:red'>Description is required</p>";
            header('Location: form.php');
            die();
        } 

        if (empty($email)) {
            $_SESSION['email'] = "<p style='color:red'>Email is required</p>";
            header('Location: form.php');
            die();
        } 
}


function validateInputsCompany() {
    
    $email = $_POST['email'];
    
    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
        return true; 
    } else {
        $_SESSION['emailValidation'] = "<p style='color:red'>Invalid email input</p>";
        header('Location: form.php');
        die();
    } 

}

function validateUrl() {
    $website = $_POST['website'];
    if (filter_var($website, FILTER_VALIDATE_URL)) {
        return true;
    } else {
        $_SESSION['urlValidation'] = "<p style='color:red'>Invalid url format</p>";
        header('Location: form.php');
        die();
    } 
}



