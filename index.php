<?php
session_start();
?>

<!DOCTYPE html>
<html>

<head>
    <title>Hackaton Group 8</title>
    <meta name='keywords' content='Hackaton, Laika, Brainster, Colony'>
    <meta name='description' content=''>
    <meta name='author' content='Gorjan Mitrushevski, Meri Neskovska, Ivan Ivanov, Igor Zdravkovski'>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width,initial-scale=1.0'>
    <!-- Latest compiled and minified BOOTSTRAP CSS -->
    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>
    <!-- Local CSS -->
    <link rel='stylesheet' type='text/css' href='style.css'>
    <!-- Font-awesome 4.7 cdn -->
    <link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'>
    <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">
</head>

<body>

      <!-- Modal -->
       <?php
      require_once 'db.php';
                $sql = 'SELECT cards.id, cards.title, cards.img_url, cards.about, categories.category, categories.category_color FROM cards JOIN categories ON cards.category_id = categories.id WHERE cards.approved = "1"';
                $stmt = $pdo->query($sql);
                $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
                
                foreach ($result as $card) {
                    

                    echo '<div class="modal fade" id="modal_'.$card['id'].'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="row">
                        <div class="col-md-4 left-sidebar-modal ">
                            <p class="whiteText ">
                                '.$card['title'].' 
                            </p>  
                            
                        </div>
                        <div class="col-md-8 float-left-container">
                            <div class="img-container-modal" style="background-image: url('.$card["img_url"].')">
                             <i class="fa fa-times fa-2x x-icon-style" data-dismiss="modal"></i>
                            </div>
                            <div class="row">
                                <div class="col-md-10 col-md-offset-1 text-modal-style">
                                    <h3>Lorem Ipsum</h3>
                                    <p>'.$card['about'].'
                                        </p>
                                        <form class="navbar-form pull-right" role="email" action="processEmail.php" method="POST">
                                                <div class="form-group email-input">
                                                    <input type="email" class="form-control" name="email1"
                                                        placeholder="Get two new looks every week">
                                                    <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                                    <button type="submit" class="btn btn-default">Find out first</button>
                                                </div>
                                            </form>
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>';
                }
        ?> 
      

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

            </div>
        </div>
    </div>
    <nav class='navbar navbar-default navbar-fixed-top'>
        <div class='container-fluid'>
            <div class='row'>
                <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                    <div class='navbar-header'>
                        <button type='button' class='navbar-toggle collapsed' data-toggle='collapse'
                            data-target='#menu-button' aria-expanded='false'>
                            <span class='sr-only'>Toggle navigation</span>
                            <span class='icon-bar'></span>
                            <span class='icon-bar'></span>
                            <span class='icon-bar'></span>
                        </button>
                        <a class="navbar-brand" href="index.php" style="padding: 0">
                            <img alt="Brand" src="./assets/img/2HRtools@4x-8.png" class="brand">
                        </a>
                    </div>
                    <div class='collapse navbar-collapse' id='menu-button'>
                        <ul class='nav navbar-nav'>
                            <li>
                                <form class='navbar-form' role='search'>
                                    <div class='form-group search-cont'>
                                        <i class="fa fa-search" id="search-btn" aria-hidden="true"
                                            style="color: #CDCDCD"></i>
                                        <input type='text' id="search-input" class='form-control' placeholder='Search'
                                            style="display:none" name="search">
                                    </div>
                                </form>
                            </li>
                        </ul>
                        <ul class='nav navbar-nav navbar-right'>
                            <li>
                                <form class='navbar-form pull-right' role='email' action="processEmail.php" method="POST">
                                    <div class='form-group email-input'>
                                        <input type="text" class="form-control" name="email"
                                            placeholder="Get two new looks every week">
                                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                        <button type="submit" class="btn btn-default">Find out first</button>
                                        <?php
                                        if (isset($_SESSION['emailEr'])) {
                                            echo $_SESSION['emailEr'];
                                        }
                                        if (isset($_SESSION['emailError'])) {
                                            echo $_SESSION['emailError'];
                                        }
                                        ?>
                                    </div>
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </nav>

    <!-- main site -->
    <div class="container-fluid">
        <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 side-filter text-left">
                <ul class="list-unstyled custom-list">
                    <li><a href="#" id="education" class="sideBarLink" data-category="HR EDUCATION">HR EDUCATION</a>
                    </li>
                    <li><a href="#" id="remote" class="sideBarLink" data-category="REMOTE TEAMS">REMOTE TEAMS</a></li>
                    <li><a href="#" id="attract" class="sideBarLink" data-category="ATTRACT TALENT">ATTRACT TALENT</a>
                    </li>
                    <li><a href="#" id="retain" class="sideBarLink" data-category="RETAIN TALENT">RETAIN TALENT</a></li>
                    <li><a href="#" id="communication" class="sideBarLink"
                            data-category="COMMUNICATION TOOLS">COMMUNICATION TOOLS</a></li>
                    <li><a href="#" id="tools" class="sideBarLink" data-category="PM TOOLS">PM TOOLS</a></li>
                    <li><a href="#" id="communities" class="sideBarLink" data-category="HR
                            COMMUNITIES">HR
                            COMMUNITIES</a></li>
                    <li><a href="#" id="hiring">HIRING</a></li>
                    <li style="display: none" class="showHide"><a href="#" id="addNew" >ADD NEW
                            COMPANY</a></li>
                </ul>

                <a href="https://www.linkedin.com/" class="logo-linkedin col-lg-12 text-left" target="_blank">
                    <i class="fa fa-linkedin fa-3x" aria-hidden="true"></i>
                </a>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9" id="main">
                <div class="row cards-cont">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-6 col-md-6 hidden-sm hidden-xs" style="margin-top:20px; margin-bottom: 40px;">
                            <img src="./assets/img/Becomeanexpertincreatingexperts.jpg" alt=""
                                class="img img-responsive">
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 find-out" style="margin-top:20px">
                            <h2>Lorem ipsum.</h2>
                            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aut nemo ipsum fugit
                                reprehenderit
                                perferendis maxime nesciunt exercitationem explicabo quam molestias!</p>
                            <button class="btn btn-info">Find out more</button>
                            <a href="" id="share">
                                <i class="fa fa-share-alt share" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
            
            <?php
                require_once 'db.php';
                $sql = 'SELECT cards.id, cards.title, cards.img_url, categories.category, categories.category_color FROM cards JOIN categories ON cards.category_id = categories.id WHERE cards.approved = "1" ORDER BY number_of_views DESC';
                $stmt = $pdo->query($sql);
                $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
               
                foreach ($result as $card) {
                    if($card["category"] == "ALL ABOUT REMOTE TEAMS"){
                        $dataCard =  "REMOTE TEAMS";
                    } else if($card["category"] == "HOW TO RETAIN") {
                        $dataCard =  "RETAIN TALENT";
                    }else{
                        $dataCard = $card["category"];
                    }

                       
                    echo '    
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 trigger-modal categoryCard" data-category = "'.$dataCard.'" id="'.$card['id'].'" onClick="updateDatabase('.$card['id'].')" >
                        <div class="myCard"  style="background-image: url('.$card["img_url"].')">
                            <div class="cardPadding">
                                <h5 class="card-paragraph">'.$card["title"].'
                                </h5>
                                <p class="card-category">'.$card["category"].'</p>
                                <div class="lenght-color" style="background-color:'.$card["category_color"].' "></div>
                            </div>
                        </div>
                    </div>';
                
                }
                 
            ?>

            </div>
            <!-- <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 text-center">
                <div class="myCard" id="laika">
                    <div class="cardPadding">
                        <img src="./assets/laika card/2Laika@4x-8.png" class="img img-responsive" alt="">
                    </div>
                </div>
            </div> -->

            <div class="col-lg-9 col-md-9 col-sm-8 col-xs-8 div-height" id="form" style="display: none">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1 container-form-style">
                        <form action="processCompany.php" method="POST"  id="form1">
                            <h4>Add your company here</h4>
                            <div class="form-group">
                                <input type="text" class="form-control input-style" name='name'
                                    placeholder="Company name" >
                            </div>
                            <?php 
                                if (isset($_SESSION['name'])) {
                                    echo $_SESSION['name'];
                                }
                            ?>
                            <div class="form-group">
                                <input type="text" placeholder="Company website" name='website'
                                    class='form-control input-style' >
                            </div>
                            <?php 
                                if (isset($_SESSION['website'])) {
                                    echo $_SESSION['website'];
                                }

                                if (isset($_SESSION['urlValidation'] )) {
                                    echo $_SESSION['urlValidation']; 
                                }
                            ?>                
                            <div class="form-group">
                                <textarea type="text" class="form-control text-area input-style"
                                    name='about' placeholder="About the company"></textarea>
                            </div>
                            <?php 
                                if (isset($_SESSION['about'])) {
                                    echo $_SESSION['about'];
                                }
                            ?> 
                            <div class="form-group">
                                <label for="">
                                    <h4>Do you want to work for this company?</h4>
                                </label>

                                <div class="form-control radio-style">
                                    <label class="btn  radio-color">
                                        <input type="radio" name="employ" value="Yes" class="radio-input">
                                        Yes
                                    </label>

                                    <label class="btn  radio-color">
                                        <input type="radio" name="employ" value="No" class="radio-input">
                                        No
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="color-change-label">
                                    <h4>Your email address</h4>
                                </label>
                                <input type="email" class="form-control input-style" name='email'
                                    placeholder="example@yahoo.com" >
                            </div>
                            <?php 
                                if (isset($_SESSION['email'])) {
                                    echo $_SESSION['email'];
                                }
                                if (isset($_SESSION['emailValidation'])) {
                                    echo $_SESSION['emailValidation'];
                                }
                            ?>  
                            <button type="submit" class="btn btn-default submit-btn-style">Submit</button>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <?php
            session_destroy();
       ?> 
    
    <script>
    function updateDatabase(id)
    {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) 
            {
                // alert(xmlhttp.responseText);
            }
        };
        xmlhttp.open("GET", "update.php?id=" +id, true);
        xmlhttp.send();
    }
    </script>

    <!-- jQuery library -->
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js'></script>
    <!-- Latest compiled JavaScript -->
    <script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script>
    <!-- Custom JavaScript -->
    <script src='main.js'></script>
</body>

</html>