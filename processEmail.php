<?php

require_once 'db.php';
require_once 'classes/userEmail.class.php';
require_once 'validation.php';

validateInputEmail();


if (validateInputEmail() == true) {
    $email = new UserEmail($_POST);

    $sql = 'INSERT INTO users_email (email) VALUES (:email)';
    $stmt = $pdo->prepare($sql);
    $stmt->execute(['email' => $email->getEmail()]);
}

header('Location: index.php');
die();
