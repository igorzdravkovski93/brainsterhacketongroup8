$(function () {

    let open = false;
    let hiringBtn = $('#hiring');
    let addNewBtn = $('.showHide');
    let searchBtn = $("#search-btn");
    let hiring = $("#hiring");
    let addCompany = $("#add-li");
    let cardsCont = $('.cards-cont');
    let clickBtn = $(".sideBarLink");

    clickBtn.click(function () {
        $(this).toggleClass('active');
        let clickCategory = $(this).attr("data-category");
        
        function openinput() {
            $("#search-input").toggle("hide");
        }
        searchBtn.on("click", openinput);
        hiring.on("click", function () {
            addCompany.show();
            this.style.color = "#FF6F61";
        })
        function showForm() {
            $("#main").hide();
            $("#form").show();
        }
        addCompany.on("click", showForm);
        if ($(this).hasClass('active')) {
            $('.categoryCard').hide();
            $(`.categoryCard[data-category='${clickCategory}']`).show();
        } else {
            $('.categoryCard').show();
        }
    });

    hiringBtn.click(function () {
        if (open !== true) {
            open = true;
            addNewBtn.show();
        } else {
            open = false;
            addNewBtn.hide();
        }
    });

    // let searchBtn = $("#search-btn");
    // let hiring = $("#hiring");
    // let addCompany = $(".showHide");
    let addNew = $("#addNew");
    function openinput() {
        $("#search-input").toggle("hide");
    }
    searchBtn.on("click", openinput);
    hiring.on("click", function () {
        $(addCompany).show();
    });
    function showForm(){
        $(".cards-cont").hide();
        $("#form").show();
        $(this).css("color","#FF6F61");
    }
    addNew.on("click", showForm);



    let triger = document.querySelectorAll(".trigger-modal");
    triger.forEach(elem => {
    elem.addEventListener("click", function(e) {

        $("#modal_"+this.id).modal('show');
    });   
  });
});
