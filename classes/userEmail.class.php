<?php

class UserEmail {
    private $email;

    public function __construct($arr) {
        $this->email = $arr['email'];
    }

    public function getEmail() {
        return $this->email;
    }

}