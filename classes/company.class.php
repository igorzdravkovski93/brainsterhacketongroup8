<?php

class Company {
    private $name;
    private $website;
    private $about;
    private $employ;
    private $email;

    public function __construct($arr) {
        $this->name = $arr['name'];
        $this->website = $arr['website'];
        $this->about = $arr['about'];
        $this->employ = $arr['employ'];
        $this->email = $arr['email'];
    }

    public function getName() {
        return $this->name;
    }
    
    public function getWebsite() {
        return $this->website;
    }

    public function getAbout() {
        return $this->about;
    }

    public function getEmploy() {
        return $this->employ;
    }

    public function getEmail() {
        return $this->email;
    }
}