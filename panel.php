<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>
    <title>Admin panel</title>
</head>
<body>
    <div class="container-fluid">
    <div class="row">
    <?php
        require_once 'db.php';
        $sql = 'SELECT cards.id, cards.company_name, cards.company_website, cards.about, cards.email FROM cards WHERE cards.approved = "0"';
        $stmt = $pdo->query($sql);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-8 col-md-offset-2 col-xs-12 ">
                    <form action="" method="POST">
                    <table class="table table-striped">
                        <tr>
                            <td>Company name</td>
                            <td>Company website</td>
                            <td>About the company</td>
                            <td>Email</td>
                            <td>Status</td>
                            <td>Approve</td>
                        </tr>';
        
                    foreach ($result as $card) {
            
                    echo '    
                        <tr>
                            <td>'.$card['company_name'].'</td>
                            <td>'.$card['company_website'].'</td>
                            <td>'.$card['about'].'</td>
                            <td>'.$card['email'].'</td>
                            <td>Not Approved</td>
                            <td><button id="myBtn" type="submit" name="approve" value="'.$card['id'].'" class="btn btn-success">Approve</button></td>
                        </tr>';
                    }
                echo '</table>
                    </form>
                   </div>
                </div>';
                    
                if (isset($_POST['approve'])) {
                $id = $_POST['approve'];
                $sql1 = 'UPDATE cards SET cards.approved = "1" WHERE cards.id=(:id)';
                $stmt1 = $pdo->prepare($sql1);
                $stmt1->execute(['id' => $id]);
                echo "<script>alert('Post approved')</script>";
                }
    ?>
    </div>
</div>
</body>
</html>