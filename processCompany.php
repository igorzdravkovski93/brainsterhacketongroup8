<?php

require_once 'db.php';
require_once 'classes/company.class.php';
require_once 'validation.php';

checkIfEmptyCompany();
validateInputsCompany();
validateUrl();


if (validateInputsCompany() == true && validateUrl() == true) {
    $company = new Company($_POST);

    $sql = 'INSERT INTO cards (company_name, company_website, about, employed, email)   VALUES (:name, :url, :about, :employed, :email)';
    $stmt = $pdo->prepare($sql);
    $stmt->execute(['name'=>$company->getName(), 'url'=>$company->getWebsite(),     'about'=>$company->getAbout(), 'employed'=>$company->getEmploy(),'email' =>      $company->getEmail()]);
    
}

header('Location: index.php');
die();
